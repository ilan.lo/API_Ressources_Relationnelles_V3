package com.cesi.cda.courspoo;

import com.cesi.cda.courspoo.Business.RessourceBusiness;
import com.cesi.cda.courspoo.Controller.Ressources.RessourcesController;
import com.cesi.cda.courspoo.DAO.Ressources.Model.Ressource;
import com.cesi.cda.courspoo.DTO.User.RessourceDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RessourceControllerTest {

    private MockMvc mockMvc;

    @Mock
    private RessourceBusiness ressourceBusiness;

    @InjectMocks
    private RessourcesController ressourcesController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(ressourcesController).build();
    }


    @Test
    @Tag("unit")
    @UnitTest
    public void testDoNothing() throws Exception {
        assert(true==true);
    }


    @Test
    @Tag("unit")
    @UnitTest
    public void testGetRessources() throws Exception {
        Ressource ressource1 = new Ressource();
        ressource1.setId(1);
        Ressource ressource2 = new Ressource();
        ressource2.setId(2);

        List<Ressource> ressources = Arrays.asList(ressource1, ressource2);

        when(ressourceBusiness.getRessources()).thenReturn(ressources);

        mockMvc.perform(get("/ressources"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[1].id").value(2));

        verify(ressourceBusiness, times(1)).getRessources();
        verifyNoMoreInteractions(ressourceBusiness);
    }

    @Test
    @Tag("unit")
    @UnitTest
    public void testGetRessourceById() throws Exception {
        Ressource ressource = new Ressource();
        ressource.setId(1);

        when(ressourceBusiness.getRessourceById(1)).thenReturn(ressource);

        mockMvc.perform(get("/ressources/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1));

        verify(ressourceBusiness, times(1)).getRessourceById(1);
        verifyNoMoreInteractions(ressourceBusiness);
    }


    @Test
    @Tag("unit")
    @UnitTest
    public void testEditRessource() throws Exception {
        Ressource ressource = new Ressource();
        ressource.setId(1);
        ressource.setTitre("Updated Resource");

        mockMvc.perform(put("/ressource")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":1,\"titre\":\"Updated Resource\"}"))
                .andExpect(status().isOk())
                .andExpect(content().string("Ressource modifiée"));

        verify(ressourceBusiness, times(1)).editRessource(ressource);
        verifyNoMoreInteractions(ressourceBusiness);
    }

    @Test
    @Tag("unit")
    @UnitTest
    public void testDeleteRessource() throws Exception {
        mockMvc.perform(delete("/ressource?id=1&proprietaire=1"))
                .andExpect(status().isOk())
                .andExpect(content().string("Ressource supprimée"));

        verify(ressourceBusiness, times(1)).deleteRessource(1, 1);
        verifyNoMoreInteractions(ressourceBusiness);
    }


}
