package com.cesi.cda.courspoo.Controller.AuthController;



import com.cesi.cda.courspoo.Business.AuthBusiness;
import com.cesi.cda.courspoo.Business.BCryptBusiness;
import com.cesi.cda.courspoo.DTO.User.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Objects;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    private AuthBusiness authBusiness;

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody UserDTO user) throws Exception {
        if(Objects.equals(user.getEmail(), "") || Objects.equals(user.getMotDePasse(), "") || Objects.equals(user.getNom(), "")) {
            return ResponseEntity.badRequest().body("Condition is empty.");
        }
        String mdp = user.getMotDePasse();
        BCryptPasswordEncoder encoder = BCryptBusiness.getEncoder();
        String encodedPassword = encoder.encode(mdp);
        boolean isMatch = encoder.matches(mdp, encodedPassword);
        user.setMotDePasse(encodedPassword);
        authBusiness.registerUser(user);
        return ResponseEntity.ok("User registered successfully, bool is "+ isMatch + " mdp is"+encodedPassword);
    }

    @PostMapping("/connect")
    public boolean connectUser(String email, String password) throws Exception {
        boolean isConnect = authBusiness.checkUser(email, password);
        return isConnect;
    }

}
