package com.cesi.cda.courspoo.DAO.Mapper;

import com.cesi.cda.courspoo.DAO.Ressources.Model.Ressource;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RessourcesMapper implements RowMapper<Ressource>{

    @Override
    public Ressource mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        Ressource ressource = new Ressource();
        ressource.setId(resultSet.getInt("Id"));
        ressource.setTitre(resultSet.getString("Titre"));
        ressource.setContenu(resultSet.getString("Contenu"));
        ressource.setProprietaire(resultSet.getInt("proprietaire"));
        ressource.setIsValidate(resultSet.getBoolean("isValidate"));
        ressource.setCreatedDate(resultSet.getString("createdDate"));
        return ressource;
    }
}
