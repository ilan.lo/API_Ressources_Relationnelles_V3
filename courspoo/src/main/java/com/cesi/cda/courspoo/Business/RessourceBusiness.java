package com.cesi.cda.courspoo.Business;

import com.cesi.cda.courspoo.DAO.Ressources.Model.Ressource;
import com.cesi.cda.courspoo.DAO.Ressources.Repository.RessourcesRepository;
import com.cesi.cda.courspoo.DTO.User.RessourceDTO;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RessourceBusiness {

    @Autowired
    RessourcesRepository ressourcesRepository;



    public List <Ressource> getRessources(){

        return ressourcesRepository.getRessources();

    }

    public Ressource getRessourceById(int id){
        return ressourcesRepository.getRessourceById(id);
    }

    public void postRessource(RessourceDTO ressource) throws Exception {
        ressourcesRepository.postRessource(ressource);

    }
    public void editRessource(Ressource ressource) throws Exception {

        Ressource existingRessource = ressourcesRepository.getRessourceById(ressource.getId());
        if (existingRessource.getProprietaire() != ressource.getProprietaire()) {
            throw new IllegalAccessException("Vous n'êtes pas autorisé à modifier cette ressource.");
        }

        // Mettre à jour la ressource
        ressourcesRepository.updateRessource(ressource);
    }


    public void deleteRessource(int id, int proprietaire) throws IllegalAccessException {
        Ressource existingRessource = ressourcesRepository.getRessourceById(id);
        if (existingRessource.getProprietaire() != proprietaire) {
            throw new IllegalAccessException("Vous n'êtes pas autorisé à supprimer cette ressource.");
        }

        // Supprimer la ressource
        ressourcesRepository.deleteRessource(id);


    }

    public void validateRessource(Ressource ressource) {

        ressourcesRepository.validateRessource(ressource);

    }
}
