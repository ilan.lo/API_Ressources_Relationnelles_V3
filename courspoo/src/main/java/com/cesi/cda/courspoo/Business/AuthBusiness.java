package com.cesi.cda.courspoo.Business;

import com.cesi.cda.courspoo.DAO.User.Model.User;
import com.cesi.cda.courspoo.DAO.User.UserRepository;
import com.cesi.cda.courspoo.DTO.User.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@Service
public class AuthBusiness {

    @Autowired
    UserRepository userRepository;

    public void registerUser(UserDTO userDTO) throws Exception {
        UserDTO user = new UserDTO();
        user.setNom(userDTO.getNom());
        user.setEmail(userDTO.getEmail());
        user.setMotDePasse(userDTO.getMotDePasse()); // Notez que le mot de passe est enregistré en clair ici
        userRepository.save(user);
    }

    public boolean checkUser(String email, String password) throws Exception {
        User user = userRepository.get(email);
        return  BCryptBusiness.getEncoder().matches(password, user.getMotDePasse());
    }




}
