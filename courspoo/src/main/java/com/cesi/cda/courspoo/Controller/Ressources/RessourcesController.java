package com.cesi.cda.courspoo.Controller.Ressources;

import com.cesi.cda.courspoo.Business.RessourceBusiness;
import com.cesi.cda.courspoo.DAO.Ressources.Model.Ressource;
import com.cesi.cda.courspoo.DTO.User.RessourceDTO;
import jakarta.validation.constraints.Max;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RessourcesController {

    @Autowired
    RessourceBusiness ressourceBusiness;

    @GetMapping("/ressources")
    public List<Ressource> getRessources(){
        return ressourceBusiness.getRessources();

    }

    @GetMapping("/ressources/{id}")
    public Ressource getRessourceById(@PathVariable int id){
        return ressourceBusiness.getRessourceById(id);
    }

    @PostMapping("/ressource")
    public ResponseEntity<?> postRessource(@RequestBody RessourceDTO ressource) throws Exception {
        ressourceBusiness.postRessource(ressource);
        return ResponseEntity.ok("Ressource added");

    }

    @PutMapping("/ressource")
    public ResponseEntity<?> editRessource (@RequestBody Ressource ressource) throws Exception {
        ressourceBusiness.editRessource(ressource);
        return ResponseEntity.ok("Ressource modifiée");
    }

    @DeleteMapping("/ressource")
    public ResponseEntity<?> deleteRessource(int id, int proprietaire) throws IllegalAccessException {
        ressourceBusiness.deleteRessource(id, proprietaire);
        return ResponseEntity.ok("Ressource supprimée");
    }

    @PutMapping("/validateRessource/{id}")
    public ResponseEntity<?> validateRessource (@PathVariable int id) throws Exception {
        Ressource ressource = ressourceBusiness.getRessourceById(id);
        ressourceBusiness.validateRessource(ressource);
        return ResponseEntity.ok("Ressource validée");
    }




}
