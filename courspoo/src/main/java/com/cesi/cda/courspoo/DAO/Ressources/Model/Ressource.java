package com.cesi.cda.courspoo.DAO.Ressources.Model;

import lombok.Data;
import lombok.Setter;

import java.util.Date;

@Data
public class Ressource {

    int Id;
    String Titre;
    String Contenu;
    int Proprietaire;
    boolean isValidate;
    String createdDate;

    public int getId() {
        return Id;
    }

    public String getTitre() {
        return Titre;
    }

    public String getContenu() {
        return Contenu;
    }

    public int getProprietaire() {
        return Proprietaire;
    }

    public boolean getIsValidate() {
        return isValidate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    // Setters
    public void setId(int Id) {
        this.Id = Id;
    }

    public void setTitre(String Titre) {
        this.Titre = Titre;
    }

    public void setContenu(String Contenu) {
        this.Contenu = Contenu;
    }

    public void setProprietaire(int Proprietaire) {
        this.Proprietaire = Proprietaire;
    }

    public void setIsValidate(boolean isValidate) {
        this.isValidate = isValidate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

}
