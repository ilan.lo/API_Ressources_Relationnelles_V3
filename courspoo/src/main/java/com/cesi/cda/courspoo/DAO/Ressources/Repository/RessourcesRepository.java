package com.cesi.cda.courspoo.DAO.Ressources.Repository;

import com.cesi.cda.courspoo.DAO.Mapper.RessourcesMapper;
import com.cesi.cda.courspoo.DAO.Ressources.Model.Ressource;
import com.cesi.cda.courspoo.DTO.User.RessourceDTO;
import com.fasterxml.jackson.annotation.JacksonInject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

import java.util.List;

@Repository
public class RessourcesRepository {

    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;

    final String SQL_GET_RESSOURCES = "SELECT * FROM RESSOURCE";
    final String SQL_GET_RESSOURCE_BY_ID = "SELECT * FROM RESSOURCE WHERE ID=:id";
    final String SQL_POST_RESSOURCE = "INSERT INTO RESSOURCE (titre, contenu, proprietaire) VALUES (:titre, :contenu, :proprietaire)";
    final String SQL_UPDATE_RESSOURCE = "UPDATE RESSOURCE SET titre = :titre, contenu = :contenu, isValidate = :isValidate, createdDate = :createdDate WHERE ID = :id AND proprietaire = :proprietaire";
    final String SQL_DELETE_RESSOURCE = "DELETE FROM RESSOURCE WHERE ID = :id";
    final String SQL_GET_RESSOURCE_LAST_MONTH = "SELECT * FROM RESSOURCE WHERE createdDate > :date";
    final String SQL_VALIDATE_RESSOURCE = "UPDATE RESSOURCE SET isValidate = 1 WHERE ID = :id";

    public List<Ressource> getRessources() throws DataAccessException {
        try {

            return jdbcTemplate.query(SQL_GET_RESSOURCES, new RessourcesMapper());
        } catch (DataAccessException e) {

            throw new RuntimeException("Erreur lors de la récupération des ressources.", e);
        }
    }
    public List<Ressource> getRessourcesLastMonth() throws DataAccessException {
        try {

            LocalDate currentDate = LocalDate.now();
            LocalDate lastMonthDate = currentDate.minusMonths(1);

            var parameters =  new MapSqlParameterSource()
                    .addValue("date", lastMonthDate);
            return jdbcTemplate.query(SQL_GET_RESSOURCE_LAST_MONTH, parameters, new RessourcesMapper());
        } catch (DataAccessException e) {

            throw new RuntimeException("Erreur lors de la récupération des ressources.", e);
        }
    }

    public Ressource getRessourceById(int id) throws DataAccessException{
        try{
            var parameters =  new MapSqlParameterSource()
                    .addValue("id", id);
            return jdbcTemplate.queryForObject(SQL_GET_RESSOURCE_BY_ID, parameters, new RessourcesMapper());

        } catch (DataAccessException e){
            throw new RuntimeException("Erreur lors de la récupération des ressources.", e);
        }
    }


    public void postRessource(RessourceDTO ressource) throws Exception {
        try{
            var parameters = new MapSqlParameterSource();

            parameters.addValue("titre", ressource.getTitre());
            parameters.addValue("contenu", ressource.getContenu());
            parameters.addValue("proprietaire", ressource.getProprietaire());

            jdbcTemplate.update(SQL_POST_RESSOURCE, parameters);

        } catch (Exception e){
            throw  new RuntimeException("Ressource non postée");
        }


    }
    public void updateRessource(Ressource ressource) throws DataAccessException {
        try {
            var parameters = new MapSqlParameterSource();
            parameters.addValue("id", ressource.getId());
            parameters.addValue("titre", ressource.getTitre());
            parameters.addValue("contenu", ressource.getContenu());
            parameters.addValue("proprietaire", ressource.getProprietaire());
            parameters.addValue("isValidate", ressource.getIsValidate() ? 1: 0);
            parameters.addValue("createdDate", ressource.getCreatedDate());


            jdbcTemplate.update(SQL_UPDATE_RESSOURCE, parameters);


        } catch (DataAccessException e) {
            throw new RuntimeException("Erreur lors de la mise à jour de la ressource.", e);
        }
    }

    public void deleteRessource(int id) {

        try {
            var parameters = new MapSqlParameterSource();
            parameters.addValue("id", id);

         jdbcTemplate.update(SQL_DELETE_RESSOURCE, parameters);


        } catch (DataAccessException e) {
            throw new RuntimeException("Erreur lors de la suppression de la ressource.", e);
        }
    }

    public void validateRessource(Ressource ressource) {

        try {
            var parameters = new MapSqlParameterSource();
            parameters.addValue("id", ressource.getId());
            parameters.addValue("isValidate", ressource.getIsValidate());


            jdbcTemplate.update(SQL_VALIDATE_RESSOURCE, parameters);


        } catch (DataAccessException e) {
            throw new RuntimeException("Erreur lors de la mise à jour de la ressource.", e);
        }
    }
}

