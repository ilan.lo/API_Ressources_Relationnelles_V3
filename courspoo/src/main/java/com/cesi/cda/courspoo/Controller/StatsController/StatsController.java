package com.cesi.cda.courspoo.Controller.StatsController;



import com.cesi.cda.courspoo.Business.AuthBusiness;
import com.cesi.cda.courspoo.Business.BCryptBusiness;
import com.cesi.cda.courspoo.Business.StatsBusiness;
import com.cesi.cda.courspoo.DAO.Ressources.Model.Ressource;
import com.cesi.cda.courspoo.DAO.Stats.Model.Stats;
import com.cesi.cda.courspoo.DTO.User.UserDTO;
import io.swagger.v3.core.util.Json;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDate;
import java.util.List;


@RestController
@RequestMapping("/api/stats")
public class StatsController {

    @Autowired
    private StatsBusiness statsBusiness;
    @GetMapping("/ressources")
    public Stats getStats() throws Exception {
        //return String.valueOf(LocalDate.now());
        return statsBusiness.getStats();
    }
}
