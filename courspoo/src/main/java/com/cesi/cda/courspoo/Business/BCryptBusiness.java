package com.cesi.cda.courspoo.Business;

import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BCryptBusiness {
    @Bean
    public static BCryptPasswordEncoder  getEncoder() {
        return new BCryptPasswordEncoder();
    }
}
