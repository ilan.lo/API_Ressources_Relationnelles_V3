package com.cesi.cda.courspoo.DTO.User;

import lombok.Data;


public class RessourceDTO {

        private String titre;
        private String contenu;
        private int proprietaire;

        // Getters and setters
        public String getTitre() {
            return titre;
        }

        public void setTitre(String titre) {
            this.titre = titre;
        }

        public String getContenu() {
            return contenu;
        }

        public void setContenu(String contenu) {
            this.contenu = contenu;
        }

        public int getProprietaire() {
            return proprietaire;
        }

        public void setProprietaire(int proprietaire) {
            this.proprietaire = proprietaire;
        }
    }

