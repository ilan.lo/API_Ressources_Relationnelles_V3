package com.cesi.cda.courspoo.DAO.Mapper;

import com.cesi.cda.courspoo.DAO.Ressources.Model.Ressource;
import com.cesi.cda.courspoo.DAO.User.Model.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
public class UserMapper implements RowMapper<User>{

    @Override
    public User mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt("ID"));
        user.setNom(resultSet.getString("NOM"));
        user.setEmail(resultSet.getString("EMAIL"));
        user.setMotDePasse(resultSet.getString("MOT_DE_PASSE"));
        return user;
    }
}
