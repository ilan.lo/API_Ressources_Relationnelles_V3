package com.cesi.cda.courspoo.DAO.User;

import com.cesi.cda.courspoo.DAO.Mapper.RessourcesMapper;
import com.cesi.cda.courspoo.DAO.Mapper.UserMapper;
import com.cesi.cda.courspoo.DAO.User.Model.User;
import com.cesi.cda.courspoo.DTO.User.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.Collections;

@Repository
public class UserRepository {

    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;
    final String sql = "INSERT INTO utilisateur (nom, email, mot_de_passe) VALUES (:nom,:email,:mot_de_passe)";
    final String sqlSelect = "SELECT * FROM utilisateur where email = :email;";
    final String sqlCount = "SELECT count(*) FROM utilisateur;";
    final String sqlCountMonth = "SELECT count(*) FROM utilisateur WHERE createdDate > :date;";

    public void save(UserDTO user) throws Exception {

        try {

            var parameters = new MapSqlParameterSource();
            parameters.addValue("nom", user.getNom());
            parameters.addValue("email", user.getEmail());
            parameters.addValue("mot_de_passe", user.getMotDePasse());

            jdbcTemplate.update(sql, parameters);
        }catch(Exception e){
            throw new Exception("enregistrement pas ok " + e);
        }
    }

    public User get(String email) throws Exception {

        try {

            var parameters = new MapSqlParameterSource();
            parameters.addValue("email", email);

            return jdbcTemplate.queryForObject(sqlSelect, parameters, new UserMapper());
        }catch(Exception e){
            throw new Exception("enregistrement pas ok " + e);
        }
    }
    public int getCount() throws Exception {

        try {

            return jdbcTemplate.queryForObject(sqlCount, Collections.emptyMap(), Integer.class);

        }catch(Exception e){
            throw new Exception("enregistrement pas ok " + e);
        }
    }
    public int getCountMonth() throws Exception {

        try {

            LocalDate currentDate = LocalDate.now();
            LocalDate lastMonthDate = currentDate.minusMonths(1);

            var parameters =  new MapSqlParameterSource()
                    .addValue("date", lastMonthDate);
            return jdbcTemplate.queryForObject(sqlCount, parameters, Integer.class);

        }catch(Exception e){
            throw new Exception("enregistrement pas ok " + e);
        }
    }
}
