package com.cesi.cda.courspoo.Business;

import com.cesi.cda.courspoo.DAO.Ressources.Model.Ressource;
import com.cesi.cda.courspoo.DAO.Ressources.Repository.RessourcesRepository;
import com.cesi.cda.courspoo.DAO.Stats.Model.Stats;
import com.cesi.cda.courspoo.DAO.User.Model.User;
import com.cesi.cda.courspoo.DAO.User.UserRepository;
import com.cesi.cda.courspoo.DTO.User.UserDTO;
import io.swagger.v3.core.util.Json;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.List;

@Service
public class StatsBusiness {

    @Autowired
    RessourcesRepository ressourcesRepository;
    @Autowired
    UserRepository usersRepository;

    public Stats getStats() throws Exception {

        List<Ressource> listRessourcesMonth = ressourcesRepository.getRessourcesLastMonth();
        List<Ressource> listRessources = ressourcesRepository.getRessources();
        int countRessourcesMonth = listRessourcesMonth.size();
        int countRessources = listRessources.size();
        int countUsers = usersRepository.getCount();
        int countUsersMonth = usersRepository.getCountMonth();

        Stats stats = new Stats();
        stats.countRessources = countRessources;
        stats.countRessourcesMonth = countRessourcesMonth;
        stats.countUsers = countUsers;
        stats.countUsersMonth = countUsersMonth;

        // Convert the JSON object to a string
        return stats;


    }
}
