DROP
    TABLE
        IF EXISTS PERSON,RENCONTRE;


CREATE
    TABLE
    RESSOURCE(
            id INT AUTO_INCREMENT,
            titre VARCHAR(100) NOT NULL,
            contenu VARCHAR(100) NOT NULL,
            proprietaire int NOT NULL,
            isValidate TINYINT DEFAULT 0,
            createdDate VARCHAR(50),


              PRIMARY KEY(id)
);

INSERT INTO RESSOURCE (titre,contenu, proprietaire, createdDate) VALUES
('Ressource 1', 'Ceci est un exemple de ressource',1, '2024-03-29'),
('Ressource 2', 'Ceci est un exemple de ressource',1, '2024-01-29'),
('Ressource 3', 'Ceci est un exemple de ressource',1, '2024-01-29');

CREATE
    TABLE
    utilisateur (
       id INT AUTO_INCREMENT PRIMARY KEY,
       nom VARCHAR(100) NOT NULL,
       email VARCHAR(100) NOT NULL UNIQUE,
       mot_de_passe VARCHAR(255) NOT NULL,
       createdDate VARCHAR(50)
    );


COMMIT;
